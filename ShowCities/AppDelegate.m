//
//  AppDelegate.m
//  ShowCities
//
//  Created by Sujith Chandran on 11/05/16.
//  Copyright © 2016 Sujith. All rights reserved.
//
#import "AppDelegate.h"
#import "LocationManagerInstance.h"
#import "AFNetworking.h"
@import GoogleMaps;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //google maps API Key
    [GMSServices provideAPIKey:@"AIzaSyBI9_i0t72TE1CckfEmMxD4gXhLYkGKXOg"];
    
    
    //Initialize location Services
      [LocationManagerInstance sharedManager];
    
    
    
    //Initialize AFNetworking
    [AFNetworkReachabilityManager sharedManager];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        if (status == AFNetworkReachabilityStatusReachableViaWiFi || status == AFNetworkReachabilityStatusReachableViaWWAN)
        {
           
            [defaults setValue:@"yes" forKey:@"isReachable"];
            
        }
        else if(status == AFNetworkReachabilityStatusNotReachable)
        {
          
            [defaults setValue:@"no" forKey:@"isReachable"];
        }
        else
        {
            [defaults setValue:@"yes" forKey:@"isReachable"];
        }
        
        [defaults synchronize];
    }];

    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
