
//
//  ListViewController.h
//  ShowCities
//
//  Created by Sujith Chandran on 11/05/16.
//  Copyright © 2016 Sujith. All rights reserved.
//

#import "BaseViewController.h"
#import "ListTableViewCell.h"

@interface ListViewController : BaseViewController<UITableViewDataSource>



@property (weak, nonatomic) IBOutlet UITableView *listTable;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;

@property(strong,nonnull)NSMutableArray *cities;


- (IBAction)actionBack:(id)sender;


@end
