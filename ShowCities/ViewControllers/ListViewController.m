//
//  ListViewController.m
//  ShowCities
//
//  Created by Sujith Chandran on 11/05/16.
//  Copyright © 2016 Sujith. All rights reserved.
//

#import "ListViewController.h"


@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listTable.dataSource= self;
    [self.listTable reloadData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//tableview methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.cities.count == 0)
    {
        self.emptyLabel.hidden = false;
        self.listTable.hidden = true;
        
    }
    else
    {
        self.emptyLabel.hidden = true;
        self.listTable.hidden = false;
   
    }
    
    return self.cities.count;
    
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ListTableViewCell *cell = [self.listTable dequeueReusableCellWithIdentifier:@"ListTableViewCell"];
    
    
    
    NSDictionary *city = [self.cities objectAtIndex:indexPath.row];
    
    
    cell.nameLabel.text = [city objectForKey:@"name"];
    cell.countryLabel.text = [city objectForKey:@"countryName"];
    
    NSString *distance = [city objectForKey:@"distance"];
    
    float dis = distance.doubleValue *1.60934 ;
    
    
    cell.distanceLabel.text = [NSString stringWithFormat:@"%0.2f Kms",dis];
    
    return cell;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true];
}


@end
