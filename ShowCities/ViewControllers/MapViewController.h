//
//  MapViewController.h
//  ShowCities
//
//  Created by Sujith Chandran on 11/05/16.
//  Copyright © 2016 Sujith. All rights reserved.
//

#import "BaseViewController.h"
#import "LocationManagerInstance.h"
@import GoogleMaps;


@interface MapViewController : BaseViewController <GMSMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong)IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *centerImagePin;

@end
