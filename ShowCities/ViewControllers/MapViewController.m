//
//  MapViewController.m
//  ShowCities
//
//  Created by Sujith Chandran on 11/05/16.
//  Copyright © 2016 Sujith. All rights reserved.
//

#import "MapViewController.h"
#import "ListViewController.h"


@interface MapViewController ()


@property(strong,nonatomic)NSString *latitude;
@property(strong,nonatomic)NSString *longitude;

@property(strong,nonatomic)GMSMarker *currentLocationMarker;

@property (nonatomic,strong) NSDictionary *responseDictionary;

@property (nonatomic,strong) NSMutableArray *cities;
@property (nonatomic,strong) UIImageView *markerImage;

@property BOOL didLoadOnce;
@property BOOL isGesture;



@end

@implementation MapViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(justGotLocation:)
                                                 name:@"justGotLocation"
                                               object:nil];

    
    
    
    self.mapView.delegate =self;
    
    self.didLoadOnce =false;
    
    self.currentLocationMarker = [[GMSMarker alloc]init];
    self.currentLocationMarker.icon= [UIImage imageNamed:@"current_position_marker"];
    self.currentLocationMarker.map = self.mapView;

    
    self.markerImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"city_marker"]];
    
    
    // Do any additional setup after loading the view.
}



-(void)fetchNearbyPlacesForCoordinate:(CLLocationCoordinate2D)coordinate{

    //http://api.geonames.org/findNearbyJSON?lat=47.3&lng=9&username=devsujith&radius=300
    
    
    if ([self isReachable] == true) {
        
        
         [self showActivityIndicatorInView:self.view withTitle:@"Loading cities nearby..."];
     
        NSString *latitude = [NSString stringWithFormat:@"%f",coordinate.latitude];
         NSString *longitude = [NSString stringWithFormat:@"%f",coordinate.longitude];
        
        
        
            NSString *url = [NSString stringWithFormat:@"http://api.geonames.org/findNearbyJSON?lat=%@&lng=%@&username=devsujith&radius=300",latitude,longitude];
        
        
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
              [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
            
            self.responseDictionary =(NSDictionary *)responseObject;
            self.cities = [self.responseDictionary objectForKey:@"geonames"];
            
            if(self.cities)
            {
                [self refreshAnnotaions];
            }
            else
            {
                [self showError];
            }
            
            
            [self removeActivityIndicatorForView:self.view];
          
        }
             failure:^(NSURLSessionTask *operation, NSError *error) {
                
                [self removeActivityIndicatorForView:self.view];
                 [self showError];
             }];

        
          }
    else
    {

        [self showNoInternetError];
        [self removeActivityIndicatorForView:self.view];
      
    }

    
    
    
    
    
    
    
}


-(void)refreshAnnotaions{
    
    //clear previous markers
    [self.mapView clear];
    
    //add current location marker.
     self.currentLocationMarker.map = self.mapView;
    
    //add new markers
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    
    
    for(NSDictionary *city in self.cities)
    {
        NSString *lat = [city objectForKey:@"lat"];
        NSString *lng = [city objectForKey:@"lng"];
        NSString *name = [city objectForKey:@"name"];
        NSString *distance = [city objectForKey:@"distance"];
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(lat.doubleValue, lng.doubleValue);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        
        bounds = [bounds includingCoordinate:marker.position];
        marker.title = name;
        
        float dis = distance.doubleValue *1.60934 ;
        
        marker.snippet = [NSString stringWithFormat:@"%0.2f Kms",dis];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.iconView = self.markerImage;
       
    
        marker.map = self.mapView;
        
        
        
        
        
    }
    

    
    
}


//location class observer
- (void) justGotLocation:(NSNotification *) notification
{
    
    
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:[[LocationManagerInstance sharedManager] currentLatitude].doubleValue longitude:[[LocationManagerInstance sharedManager] currentLongitude].doubleValue];
    
    self.currentLocationMarker.position = loc.coordinate;
    
if(!self.didLoadOnce)
{

    
    self.mapView.camera = [GMSCameraPosition cameraWithTarget:loc.coordinate zoom:15];
    
     [self fetchNearbyPlacesForCoordinate:loc.coordinate];
    
    //s/elf.mapView
    
    self.didLoadOnce = true;
}
    
                           
                           
    
}


//GMSmapview delegates

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    
    
    self.isGesture = gesture;
    self.centerImagePin.alpha = 0.5;
    
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
     if(self.isGesture)
    {
    
    [self fetchNearbyPlacesForCoordinate:position.target];
    }
    
     self.centerImagePin.alpha = 1;
    
}


- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    mapView.selectedMarker = marker;
    return TRUE;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"showList"])
    {
          ListViewController *vc = segue.destinationViewController;
        vc.cities = self.cities;
    }
    
}



@end
